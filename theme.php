<?php global $Cms ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="icon" type="image/png" href="/themes/fresh/img/favicon.png" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700;800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" href="/themes/fresh/css/app.css">
    <link rel="stylesheet" type="text/css" href="/themes/fresh/css/main.css">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="title" content="<?= $Cms->get('config', 'siteTitle') ?> - <?= $Cms->page('title') ?>" />
    <meta name="description" content="<?= $Cms->page('description') ?>">
    <meta name="keywords" content="<?= $Cms->page('keywords') ?>">

    <meta property="og:url" content="<?= $this->url() ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="<?= $Cms->get('config', 'siteTitle') ?>" />
    <meta property="og:title" content="<?= $Cms->page('title') ?>" />
    <meta name="twitter:site" content="<?= $this->url() ?>" />
    <meta name="twitter:title" content="<?= $Cms->get('config', 'siteTitle') ?> - <?= $Cms->page('title') ?>" />
    <meta name="twitter:description" content="<?= $Cms->page('description') ?>" />

    <title><?= $Cms->get('config', 'siteTitle') ?> - <?= $Cms->page('title') ?></title>

    <link rel="stylesheet" rel="preload" as="style" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <?= $Cms->css() ?>
</head>

<body class="d-flex flex-column">
<?= $Cms->settings() ?>
<?= $Cms->alerts() ?>
<section class="hero is-fullheight is-default is-bold">
    <nav x-data="initNavbar()" class="navbar is-fresh is-transparent no-shadow" role="navigation"
         aria-label="main navigation">
        <div class="container">
            <div class="navbar-brand">
                <a class="navbar-item" href="/">
                    <img src="/themes/fresh/img/logo/fresh-alt.svg" alt="" width="112" height="28">
                </a>

                <a @click="openSidebar()" class="navbar-item is-hidden-desktop is-hidden-tablet">
                    <div id="menu-icon-wrapper" class="menu-icon-wrapper" style="visibility: visible;" :class="{
                                'open': $store.app.isSiderbarOpen,
                                '': !$store.app.isSiderbarOpen
                            }">
                        <svg width="1000px" height="1000px">
                            <path class="path1"
                                  d="M 300 400 L 700 400 C 900 400 900 750 600 850 A 400 400 0 0 1 200 200 L 800 800"></path>
                            <path class="path2" d="M 300 500 L 700 500"></path>
                            <path class="path3"
                                  d="M 700 600 L 300 600 C 100 600 100 200 400 150 A 400 380 0 1 1 200 800 L 800 200"></path>
                        </svg>
                        <button id="menu-icon-trigger" class="menu-icon-trigger"></button>
                    </div>
                </a>

                <div class="navbar-burger" @click="openMobileMenu()">
                        <span class="menu-toggle">
                            <span class="icon-box-toggle" :class="{
                                    'active': mobileOpen,
                                    '': !mobileOpen
                                }">
                                <span class="rotate">
                                    <i class="icon-line-top"></i>
                                    <i class="icon-line-center"></i>
                                    <i class="icon-line-bottom"></i>
                                </span>
                            </span>
                </div>
            </div>

            <div id="navbar-menu" class="navbar-menu is-static" :class="{
                        'is-active': mobileOpen,
                        '': !mobileOpen
                    }">

                <div class="navbar-start">
                    <a @click="openSidebar()" class="navbar-item is-hidden-mobile">
                        <div id="menu-icon-wrapper" class="menu-icon-wrapper" style="visibility: visible;" :class="{
                                    'open': $store.app.isSiderbarOpen,
                                    '': !$store.app.isSiderbarOpen
                                }">
                            <svg width="1000px" height="1000px">
                                <path class="path1"
                                      d="M 300 400 L 700 400 C 900 400 900 750 600 850 A 400 400 0 0 1 200 200 L 800 800">
                                </path>
                                <path class="path2" d="M 300 500 L 700 500"></path>
                                <path class="path3"
                                      d="M 700 600 L 300 600 C 100 600 100 200 400 150 A 400 380 0 1 1 200 800 L 800 200">
                                </path>
                            </svg>
                            <button id="menu-icon-trigger" class="menu-icon-trigger"></button>
                        </div>
                    </a>
                </div>

                <div class="navbar-end">
                    <?= $Cms->menu() ?>
                </div>
            </div>
        </div>
    </nav>
    <nav x-data="initNavbar()"  x-on:scroll.window="scroll()" id="navbar-clone" class="navbar is-fresh is-transparent" role="navigation" aria-label="main navigation" :class="{
                'is-active': scrolled,
                '': !scrolled
            }">
        <div class="container">
            <div class="navbar-brand">
                <a class="navbar-item" href="https://cssninja.io">
                    <img src="/themes/fresh/img/logo/fresh-alt.svg" alt="" width="112" height="28">
                </a>

                <a @click="openSidebar()" class="navbar-item is-hidden-desktop is-hidden-tablet">
                    <div id="menu-icon-wrapper" class="menu-icon-wrapper" style="visibility: visible;" :class="{
                                'open': $store.app.isSiderbarOpen,
                                '': !$store.app.isSiderbarOpen
                            }">
                        <svg width="1000px" height="1000px">
                            <path class="path1" d="M 300 400 L 700 400 C 900 400 900 750 600 850 A 400 400 0 0 1 200 200 L 800 800"></path>
                            <path class="path2" d="M 300 500 L 700 500"></path>
                            <path class="path3" d="M 700 600 L 300 600 C 100 600 100 200 400 150 A 400 380 0 1 1 200 800 L 800 200"></path>
                        </svg>
                        <button id="menu-icon-trigger" class="menu-icon-trigger"></button>
                    </div>
                </a>

                <div class="navbar-burger" @click="openMobileMenu()">
                        <span class="menu-toggle">
                            <span class="icon-box-toggle" :class="{
                                    'active': mobileOpen,
                                    '': !mobileOpen
                                }">
                                <span class="rotate">
                                    <i class="icon-line-top"></i>
                                    <i class="icon-line-center"></i>
                                    <i class="icon-line-bottom"></i>
                                </span>
                            </span>
                        </span>
                </div>
            </div>

            <div id="cloned-navbar-menu" class="navbar-menu is-fixed" :class="{
                        'is-active': mobileOpen,
                        '': !mobileOpen
                    }">

                <div class="navbar-start">
                    <a @click="openSidebar()" class="navbar-item is-hidden-mobile">
                        <div id="cloned-menu-icon-wrapper" class="menu-icon-wrapper" style="visibility: visible;" :class="{
                                    'open': $store.app.isSiderbarOpen,
                                    '': !$store.app.isSiderbarOpen
                                }">
                            <svg width="1000px" height="1000px">
                                <path class="path1" d="M 300 400 L 700 400 C 900 400 900 750 600 850 A 400 400 0 0 1 200 200 L 800 800"></path>
                                <path class="path2" d="M 300 500 L 700 500"></path>
                                <path class="path3" d="M 700 600 L 300 600 C 100 600 100 200 400 150 A 400 380 0 1 1 200 800 L 800 200"></path>
                            </svg>
                            <button id="cloned-menu-icon-trigger" class="menu-icon-trigger"></button>
                        </div>
                    </a>
                </div>

                <div class="navbar-end">
                    <?= $Cms->menu() ?>
                </div>
            </div>
        </div>
    </nav>

    <section class="container mt-5 mb-5">
        <div class="row">
            <div class="col-lg-12 my-auto text-center padding40">
                <?= $Cms->page('content') ?>

            </div>
        </div>
    </section>

</section>

<section class="container-fluid mt-5 mb-5 flex-grow">
    <div class="row customBackground">
        <div class="col-lg-12 my-auto text-center padding40 resetTextRotation">
            <?= $Cms->block('subside') ?>

        </div>
    </div>
</section>

<?= $Cms->freshFooter() ?>
<!-- Back To Top Button -->
<div x-data="initBackToTop()" x-on:scroll.window="scroll($event)" @click="goTop($event)" id="backtotop"><a href="javascript:" :class="{
        'visible': scrolled,
        '': !scrolled
    }"></a></div>    <div x-data="initSidebar()" class="sidebar" :class="{
            'is-active': $store.app.isSiderbarOpen,
            '': !$store.app.isSiderbarOpen
        }">
    <div class="sidebar-header">
        <img src="/themes/fresh/img/logo/fresh-square.svg">
        <a @click="closeSidebar()" class="sidebar-close" href="javascript:void(0);"><i data-feather="x"></i></a>
    </div>
    <div class="inner">
        <ul class="sidebar-menu">
            <li><span class="nav-section-title"></span></li>
            <li @click="openSidebarMenu($event)" data-menu="sidebar-menu-1" class="have-children" :class="{
                        'active': openedMenu === 'sidebar-menu-1',
                        '': openedMenu != 'sidebar-menu-1'
                    }">
                <a href="#">
                    <span class="material-icons">apps</span>
                    <span>Apps</span>
                </a>
                <ul x-show.transition="openedMenu === 'sidebar-menu-1'">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Account</a></li>
                    <li><a href="#">Documents</a></li>
                </ul>
            </li>
            <li @click="openSidebarMenu($event)" data-menu="sidebar-menu-2" class="have-children" :class="{
                        'active': openedMenu === 'sidebar-menu-2',
                        '': openedMenu != 'sidebar-menu-2'
                    }">
                <a href="#">
                    <span class="material-icons">duo</span>
                    <span>Messages</span>
                </a>
                <ul x-show.transition="openedMenu === 'sidebar-menu-2'">
                    <li><a href="#">Inbox</a></li>
                    <li><a href="#">Compose</a></li>
                    <li><a href="#">Video</a></li>
                </ul>
            </li>
            <li @click="openSidebarMenu($event)" data-menu="sidebar-menu-3" class="have-children" :class="{
                        'active': openedMenu === 'sidebar-menu-3',
                        '': openedMenu != 'sidebar-menu-3'
                    }">
                <a href="#">
                    <span class="material-icons">insert_photo</span>
                    <span>Media</span>
                </a>
                <ul x-show.transition="openedMenu === 'sidebar-menu-3'">
                    <li><a href="#">Library</a></li>
                    <li><a href="#">Upload</a></li>
                </ul>
            </li>
            <li @click="openSidebarMenu($event)" data-menu="sidebar-menu-4" class="have-children" :class="{
                        'active': openedMenu === 'sidebar-menu-4',
                        '': openedMenu != 'sidebar-menu-4'
                    }">
                <a href="#">
                    <span class="material-icons">policy</span>
                    <span>Settings</span>
                </a>
                <ul x-show.transition="openedMenu === 'sidebar-menu-4'">
                    <li><a href="#">User settings</a></li>
                    <li><a href="#">App settings</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<script type="module" src="https://cdnjs.cloudflare.com/ajax/libs/ionicons/5.5.3/esm/ionicons.min.js" integrity="sha512-yvefMpqlYCpHgveijhRrO36/NQlRdasaGIMXXqxTw0gNrQcMNrCf89/UOXcrNAZgAoPs1cHGA9o9Fk7MfxdC8w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="/themes/fresh/js/bundle.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js" integrity="sha512-2rNj2KJ+D8s1ceNasTIex6z4HWyOnEYLVC3FigGOmyQCZc2eBXKgOxQmo3oKLHyfcj53uz4QMsRCWNbLd32Q1g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js" integrity="sha384-skAcpIdS7UcVUC05LJ9Dxay8AXcDYfBJqt1CJ85S/CFujBsIzCIv+l9liuYLaMQ/" crossorigin="anonymous"></script>
<?= $Cms->js() ?>
</body>
</html>